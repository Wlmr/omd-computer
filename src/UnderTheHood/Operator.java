package UnderTheHood;

import UserFriendly.Memory;

/**
 * Created by Wilmer on 2016-09-18.
 */
public interface Operator {

    Word getWord(Memory memory);
    String toString();


}
