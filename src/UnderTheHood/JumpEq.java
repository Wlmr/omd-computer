package UnderTheHood;

import UserFriendly.Memory;

/**
 * Created by Wilmer on 2016-09-18.
 */
public class JumpEq implements Instruction{

    private int index;
    private Operator op1;
    private Operator op2;

    public JumpEq(int index, Operator op1, Operator op2) {
        this.index = index;
        this.op1 = op1;
        this.op2 = op2;
    }

    @Override
    public int execute(Memory memory, int counter) {
        return op1.getWord(memory).equals(op2.getWord(memory)) ? index : ++counter;
    }


    public String toString(){
        return "JEQ "+" "+index+" "+String.valueOf(op1)+" "+String.valueOf(op2)+"\n";
    }
}
