package UnderTheHood;

import java.util.ArrayList;

/**
 * Created by Wilmer on 2016-09-17.
 */
public abstract class Program extends ArrayList<Instruction> {
    public String toString(){
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i<super.size(); i++){
            sb.append(i+" "+super.get(i).toString());
        }
        return sb.toString();
    }
}
