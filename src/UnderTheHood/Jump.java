package UnderTheHood;

import UserFriendly.Memory;

/**
 * Created by Wilmer on 2016-09-18.
 */
public class Jump implements Instruction{

    private int index;

    public Jump(int index){
        this.index = index;
    }
    @Override
    public int execute(Memory memory, int counter) {
        return index;
    }
    public String toString(){
        return "JMP "+String.valueOf(index)+"\n";
    }
}
