package UnderTheHood;

import UserFriendly.Memory;

/**
 * Created by Wilmer on 2016-09-20.
 */

public abstract class AritmOperation implements Instruction {

    protected Operator op1;
    protected Operator op2;
    protected Address address;

    public AritmOperation(Operator op1, Operator op2, Address adr){
        this.op1 = op1;
        this.op2 = op2;
        address = adr;
    }

    protected abstract Word eval(Memory memory);

    @Override
    public int execute(Memory memory, int counter) {
        address.setWord(eval(memory), memory);
        return ++counter;
    }

    public String toString(){
        return String.valueOf(op1)+" "+String.valueOf(op2)+" "+String.valueOf(address)+"\n";
    }
}
