package UnderTheHood;

import UserFriendly.Memory;

/**
 * Created by Wilmer on 2016-09-18.
 */
public class Address implements Operator {

    private int index;


    public Address(int index){
        this.index = index;
    }



    @Override
    public Word getWord(Memory memory) {
        return memory.getWordAt(index);
    }

    public void setWord(Word word,Memory memory) {
        memory.setWordAt(index,word);
    }

    @Override
    public String toString(){
        return "["+String.valueOf(index)+"]";
    }
}
