package UnderTheHood;

import UserFriendly.Memory;

/**
 * Created by Wilmer on 2016-09-18.
 */
public interface Word extends Operator {

    Word getWord(Memory memory);
    Word mul(Word word);
    Word add(Word word);
    boolean equals(Object obj);
    String toString();

}
