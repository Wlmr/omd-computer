package UnderTheHood;

import UserFriendly.Memory;

/**
 * Created by Wilmer on 2016-09-18.
 */
public class LongWord implements Word{
    private Long value;

    public LongWord (long value) {this.value = value;}


    private Long getValue() { return value; }

    @Override
    public Word mul(Word word) {
        return new LongWord(this.value*  ((LongWord)(word)).getValue());
    }

    @Override
    public Word add(Word word) {
        return new LongWord(this.value+ ((LongWord)(word)).getValue());
    }

    @Override
    public Word getWord(Memory memory) {
        return this;
    }



    @Override
    public boolean equals(Object obj) {
        return  this.value == ((LongWord) obj).getValue();

    }

    @Override
    public String toString(){
        return String.valueOf(value);
    }
}
