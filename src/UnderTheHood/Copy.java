package UnderTheHood;

import UserFriendly.Memory;

public class Copy implements Instruction {

    private Operator op;
    private Address adr;

    public Copy(Operator op, Address adr){
        this.op = op;
        this.adr = adr;
    }

    @Override
    public int execute(Memory memory, int counter) {
        adr.setWord(op.getWord(memory),memory);
        return ++counter;
    }
    public String toString(){
        return "CPY "+String.valueOf(op)+" "+String.valueOf(adr)+"\n";
    }
}
