package UnderTheHood;

import UserFriendly.Memory;

/**
 * Created by Wilmer on 2016-09-18.
 */
public class Mul extends AritmOperation {


    public Mul(Operator op1, Operator op2, Address adr) {
        super(op1, op2, adr);
    }

    @Override
    public Word eval(Memory memory) {
       return op1.getWord(memory).mul(op2.getWord(memory));
    }


    public String toString(){
        return "MUL "+super.toString();
    }
}
