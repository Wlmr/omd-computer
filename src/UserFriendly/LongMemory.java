package UserFriendly;

import UnderTheHood.LongWord;
import UnderTheHood.Word;

/**
 * Created by Wilmer on 2016-09-17.
 */
public class LongMemory implements Memory{
    public LongWord[] memory;

    public LongMemory(int memorySize){
        memory = new LongWord[memorySize];
    }

    @Override
    public LongWord getWordAt(int i) {
        return memory[i];
    }

    @Override
    public void setWordAt(int i, Word word) {
        memory[i] = (LongWord)word;
    }
}
