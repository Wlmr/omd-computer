package UserFriendly;

import UnderTheHood.Word;

/**
 * Created by Wilmer on 2016-09-17.
 */
public interface Memory {
    Word getWordAt(int i);
    void setWordAt(int i,Word word);
}
